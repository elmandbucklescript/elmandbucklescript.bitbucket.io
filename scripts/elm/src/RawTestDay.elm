module RawTestDay exposing (RawTestDay, decoder)

import Json.Decode exposing (Decoder, float, string)
import Json.Decode.Pipeline

type alias RawTestDay =
    { date : String
    , day : String
    , total : String
    , positive : String
    }

decoder : Decoder RawTestDay
decoder = Json.Decode.succeed RawTestDay
    |> Json.Decode.Pipeline.optional "date" string "Unknown"
    |> Json.Decode.Pipeline.optional "day" string "Unknown"
    |> Json.Decode.Pipeline.required "people_tested_total" string
    |> Json.Decode.Pipeline.required "people_positive_total" string

    |> log "Raw: "


log : String -> Decoder a -> Decoder a
log message =
    Json.Decode.map (Debug.log message)
