module Msg exposing (..)

import Json.Encode

type Msg = SendString String
    | UpdateString String
