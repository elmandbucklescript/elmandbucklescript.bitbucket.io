open ElmES6;

/* setup: simple JS dom interop */
[@bs.val] [@bs.scope "document"]
external getElementById: string => Dom.element = "getElementById";

[@bs.get] external getValue: Dom.element => string = "value";
[@bs.set] external setValue: (Dom.element, string) => unit = "value";

[@bs.set]
external setOnInput: (Dom.element, Dom.event => unit)
  => unit
  = "oninput";

[@bs.get]
external getTarget: Dom.event => Dom.element = "target";


/* get input element */
let inputReason: Dom.element = getElementById("input-reason");

/* ELM INTEROP */

/* declare ports */

module Ports = {
  type t = {
    toElm: Elm.sendable(string),
    toReason: Elm.subscribable(string)
  };
};

/* get app */

let app: Elm.app(Ports.t) =
  Elm.Main.init({ node: getElementById("elm-target") });

/* wire up ports: BuckleScript-y way */

//setOnInput(inputReason, str => app.ports.toElm -> Elm.send(str));

inputReason
  -> setOnInput(event => app.ports.toElm
                           -> Elm.send(event
                                         -> getTarget
                                         -> getValue));

app.ports.toReason
  -> Elm.subscribe(str => setValue(inputReason, str));

